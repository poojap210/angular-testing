import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../services/customer.service';
import { Customer } from '../customer';
import { Observable } from "rxjs";

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  customer: Customer = new Customer();
  customerList: any=[];
  submitted = false;
  public currentUser;

  constructor(private customerService: CustomerService,
    private router: Router) {
      this.currentUser = localStorage.getItem('currentUser')? JSON.parse(localStorage.getItem('currentUser')) : '';
      console.log("abc");
     }

  ngOnInit() {
    this.customerList = this.customerService.customers;
    console.log("list "+ this.customerList);
    this.reloadData();
  }
  reloadData() {
   
  }
  newCustomer(): void {
    this.submitted = false;
    this.customer = new Customer();
  }

  save() {
    this.customerService.createCustomer(this.customer)
      .subscribe(data => console.log(data), error => console.log(error));
    this.customer = new Customer();
    //this.gotoList();
  }


  onSubmit() {
    this.submitted = true;
    this.save();    
  }

}
