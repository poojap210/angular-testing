export class Customer {
    id: number;
    firstName: string;
    lastName: string;
    address: string;
    city:string;
    state:string;
    orderTotal:number;
    emailId: string;
    active: boolean;
}