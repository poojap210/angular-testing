import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  
  }
  logIn() {
    console.log(this.email);
    console.log(this.password);
    if (this.email == "admin" && this.password == "123") {
      this.router.navigate["/home"];
    }
    else {
      alert("Please enter valid details");
    }
  }
}
