import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private baseUrl = 'http://localhost:8080/springboot-crud-rest/api/v1/Customers';

  constructor(private http: HttpClient) { }

  getCustomer(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createCustomer(Customer: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, Customer);
  }

  updateCustomer(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteCustomer(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  // getCustomersList():any[] {
    customers: [
    {
      "customer_id":1,
      "first_name":"Pooja",
      "last_name":"Patil",
      "address":"pune",
      "city":"Pune",
      "state":"Maharatra1",
      "orderTotal":25
    },
    {
      "customer_id":2,
      "first_name":"Pooje",
      "last_name":"Patilin",
      "address":"kolhapur",
      "city":"Pune",
      "state":"Maharatra",
      "orderTotal":20
    }]
    // return customers;
    //return this.http.get(`${this.baseUrl}`);
  // }
}